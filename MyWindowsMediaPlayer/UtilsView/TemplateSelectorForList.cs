﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using MyWindowsMediaPlayer.ViewModel;
using MyWindowsMediaPlayer.Model;

namespace MyWindowsMediaPlayer.UtilsView
{
    public class TemplateSelectorForList : DataTemplateSelector
    {
        public DataTemplate MusicTemplate { get; set; }
        public DataTemplate ImageTemplate { get; set; }
        public DataTemplate VideoTemplate { get; set; }
        public DataTemplate NotEditableTemplate { get; set; }
        public DataTemplate DefaultTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            PlaylistViewModel data = item as PlaylistViewModel;

            if (data.Editable == false)
                return NotEditableTemplate;
            if (data.DataType == typeof(Music))
                return MusicTemplate;
            if (data.DataType == typeof(MyWindowsMediaPlayer.Model.Image))
                return ImageTemplate;
            if (data.DataType == typeof(Video))
                return VideoTemplate;
            return DefaultTemplate;
        }
    }
}
