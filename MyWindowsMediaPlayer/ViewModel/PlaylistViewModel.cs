﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using System.Collections.ObjectModel;
using MyWindowsMediaPlayer.Model;

namespace MyWindowsMediaPlayer.ViewModel
{
    public class PlaylistViewModel : ViewModelBase
    {
        private string _name = string.Empty;
        public Type DataType { get; set; }
        public ObservableCollection<IMedia> Items { get; set; }
        public bool Editable { get; set; }

        private const string _propertyNameName = "Name";
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (Editable == true)
                {
                    _name = value;
                    RaisePropertyChanged(_propertyNameName);
                }
            }
        }

        public PlaylistViewModel()
        {
            Items = new ObservableCollection<IMedia>();
        }

        public PlaylistViewModel(string name, Type type, bool editable = true)
        {
            Items = new ObservableCollection<IMedia>();
            _name = name;
            Editable = editable;
            DataType = type;
        }

        public PlaylistViewModel(SerializablePlaylistMusic playlist)
        {
            _name = playlist.Name;
            DataType = typeof(Music);
            Items = new ObservableCollection<IMedia>();
            Editable = playlist.Editable;
            foreach (Music elem in playlist.Items)
                Items.Add(elem);
        }

        public PlaylistViewModel(SerializablePlaylistVideo playlist)
        {
            _name = playlist.Name;
            DataType = typeof(Video);
            Items = new ObservableCollection<IMedia>();
            Editable = playlist.Editable;
            foreach (Video elem in playlist.Items)
                Items.Add(elem);
        }

        public PlaylistViewModel(SerializablePlaylistImage playlist)
        {
            _name = playlist.Name;
            DataType = typeof(Image);
            Items = new ObservableCollection<IMedia>();
            Editable = playlist.Editable;
            foreach (Image elem in playlist.Items)
                Items.Add(elem);
        }
    }
}