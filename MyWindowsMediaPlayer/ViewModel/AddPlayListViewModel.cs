﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using MyWindowsMediaPlayer.Message;
using System.Windows.Input;
using System.Windows.Controls;
using MyWindowsMediaPlayer.Model;
using System.Windows;

namespace MyWindowsMediaPlayer.ViewModel
{
    public class AddPlayListViewModel : ViewModelBase
    {
        public string PlayListName { get; set; }
        private Type _playlistType = typeof(Music);

        #region ICommands
        private ICommand _relayCommandButtonClose;
        public ICommand RelayCommandButtonClose
        {
            get
            {
                return _relayCommandButtonClose;
            }
            set
            {
                _relayCommandButtonClose = value;
            }
        }

        private ICommand _relayCommandButtonAdd;
        public ICommand RelayCommandButtonAdd
        {
            get
            {
                return _relayCommandButtonAdd;
            }
            set
            {
                _relayCommandButtonAdd = value;
            }
        }

        private ICommand _relayCommandRadioButton;
        public ICommand RelayCommandRadioButton
        {
            get
            {
                return _relayCommandRadioButton;
            }
            set
            {
                _relayCommandRadioButton = value;
            }
        }
        #endregion

        public AddPlayListViewModel()
        {
            PlayListName = string.Empty;
            _relayCommandButtonAdd = new RelayCommand(new Action(AddPlaylist));
            _relayCommandButtonClose = new RelayCommand(new Action(Close));
            _relayCommandRadioButton = new RelayCommand<RadioButton>(RadioButton);
            Messenger.Default.Register<BooleanResponseMessage>(this, new Action<BooleanResponseMessage>(OnResponseAskNewPlayList));
        }

        public void OnResponseAskNewPlayList(BooleanResponseMessage e)
        {
            if (e.Response == true)
            {
                Messenger.Default.Send<CloseWindowMessage>(new CloseWindowMessage()
                {
                    SendWindowInfos = true,
                    Type = _playlistType,
                    Name = PlayListName
                });
            }
            else
                MessageBox.Show("Invalid Name", "Error");
        }

        public void AddPlaylist()
        {
            if (PlayListName == string.Empty)
                MessageBox.Show("Playlist name can't be empty", "Error");
            else
                Messenger.Default.Send<RequestNewPlaylistMessage>(new RequestNewPlaylistMessage(PlayListName));
            _playlistType = typeof(Music);
        }

        public void Close()
        {
            Messenger.Default.Send<CloseWindowMessage>(new CloseWindowMessage()
                {
                    SendWindowInfos = false
                });
        }

        //MVVM MOYEN
        public void RadioButton(RadioButton e)
        {
            switch (e.Content as string)
            {
                case "Music":
                    _playlistType = typeof(Music);
                    return;
                case "Video":
                    _playlistType = typeof(Video);
                    return;
                case "Image":
                    _playlistType = typeof(MyWindowsMediaPlayer.Model.Image);
                    return;
            }
        }
    }
}
