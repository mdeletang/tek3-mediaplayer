﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using MyWindowsMediaPlayer.Message;
using MyWindowsMediaPlayer.Model;
using Microsoft.Win32;
using System.Collections.Generic;

namespace MyWindowsMediaPlayer.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        #region Gestion des playlists et de la playlist sélectionnée

        private ObservableCollection<PlaylistViewModel> _playlists = new ObservableCollection<PlaylistViewModel>();
        public ObservableCollection<PlaylistViewModel> Playlists
        {
            get
            {
                return _playlists;
            }
        }

        private PlaylistViewModel _playlistMusicSelected;
        public PlaylistViewModel PlaylistMusicSelected
        {
            get
            {
                return _playlistMusicSelected;
            }
            set
            {
                _playlistMusicSelected = value;
                RaisePropertyChanged("PlaylistMusicSelected");
            }
        }

        private PlaylistViewModel _playlistVideoSelected;
        public PlaylistViewModel PlaylistVideoSelected
        {
            get
            {
                return _playlistVideoSelected;
            }
            set
            {
                _playlistVideoSelected = value;
                RaisePropertyChanged("PlaylistVideoSelected");
            }
        }

        private PlaylistViewModel _playlistImageSelected;
        public PlaylistViewModel PlaylistImageSelected
        {
            get
            {
                return _playlistImageSelected;
            }
            set
            {
                _playlistImageSelected = value;
                RaisePropertyChanged("PlaylistImageSelected");
            }
        }

        private PlaylistViewModel _playlistPlaying;
        private IMedia _itemPlaying;

        #endregion

        #region Gestion Buttons

        private ICommand _relayCommandButtonPrev;
        public ICommand RelayCommandButtonPrev
        {
            get
            {
                return _relayCommandButtonPrev;
            }
            set
            {
                _relayCommandButtonPrev = value;
            }
        }

        private ICommand _relayCommandButtonNext;
        public ICommand RelayCommandButtonNext
        {
            get
            {
                return _relayCommandButtonNext;
            }
            set
            {
                _relayCommandButtonNext = value;
            }
        }

        private ICommand _relayCommandButtonNewPlayList;
        public ICommand RelayCommandButtonNewPlayList
        {
            get
            {
                return _relayCommandButtonNewPlayList;
            }
            set
            {
                _relayCommandButtonNewPlayList = value;
            }
        }

        #endregion

        #region Gestion Search TextBox

        private ICommand _relayCommandSearchDone;
        public ICommand RelayCommandSearchDone
        {
            get
            {
                return _relayCommandSearchDone;
            }
            set
            {
                _relayCommandSearchDone = value;
            }
        }

        private void SearchDone(object param)
        {
            if (param == null)
                return;
            String tmp = param as String;
        }


        #endregion

        #region Gestion du theme

        private System.Windows.Media.Brush _background;
        public System.Windows.Media.Brush BackGround
        {
            get
            {
                return _background;
            }
            set
            {
                _background = value;
                RaisePropertyChanged("BackGround");
            }
        }

        private ICommand _relayCommandButtonBlue;
        public ICommand RelayCommandButtonBlue
        {
            get
            {
                return _relayCommandButtonBlue;
            }
            set
            {
                _relayCommandButtonBlue = value;
            }
        }

        private ICommand _relayCommandButtonGreen;
        public ICommand RelayCommandButtonGreen
        {
            get
            {
                return _relayCommandButtonGreen;
            }
            set
            {
                _relayCommandButtonGreen = value;
            }
        }

        private ICommand _relayCommandButtonRed;
        public ICommand RelayCommandButtonRed
        {
            get
            {
                return _relayCommandButtonRed;
            }
            set
            {
                _relayCommandButtonRed = value;
            }
        }

        private void SetBlue()
        {
            BackGround = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.LightBlue);
        }

        private void SetGreen()
        {
            BackGround = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.LightGreen);
        }

        private void SetRed()
        {
            BackGround = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.PaleVioletRed);
        }

        #endregion

        #region Gestion du clic sur la playlist

        private ICommand _relayCommandPlaylistClicked;
        public ICommand RelayCommandPlaylistClicked
        {
            get
            {
                return _relayCommandPlaylistClicked;
            }
            set
            {
                _relayCommandPlaylistClicked = value;
            }
        }

        private void PlaylistClicked(object param)
        {
            if (param == null)
                return;
            PlaylistViewModel tmp = param as PlaylistViewModel;
            if (tmp.DataType == typeof(Video))
            {
                PlaylistVideoSelected = param as PlaylistViewModel;
                Messenger.Default.Send<ChangeVisibilityMessage>(new ChangeVisibilityMessage(false, true, false));
            }
            else if (tmp.DataType == typeof(Music))
            {
                PlaylistMusicSelected = param as PlaylistViewModel;
                Messenger.Default.Send<ChangeVisibilityMessage>(new ChangeVisibilityMessage(true, false, false));
            }
            else if (tmp.DataType == typeof(Image))
            {
                PlaylistImageSelected = param as PlaylistViewModel;
                Messenger.Default.Send<ChangeVisibilityMessage>(new ChangeVisibilityMessage(false, false, true));
            }
        }
        #endregion


        #region Gestion création playlist

        private void NewPlayList()
        {
            AddPlaylistView v = new AddPlaylistView();
            v.ShowDialog();
        }

        private void OnCreateNewPlayList(NewPlayListMessage e)
        {
            _playlists.Add(new PlaylistViewModel(e.Name, e.Type));
        }

        private void OnRequestAddPlaylist(RequestNewPlaylistMessage e)
        {
            foreach (PlaylistViewModel elem in _playlists)
            {
                if (elem.Name == e.PlayListName)
                {
                    Messenger.Default.Send<BooleanResponseMessage>(new BooleanResponseMessage(false)
                    {
                        MessageData = "createPlayList"
                    });
                    return;
                }
            }
            Messenger.Default.Send<BooleanResponseMessage>(new BooleanResponseMessage(true)
            {
                MessageData = "createPlaylist"
            });
        }

        #endregion


        #region Gestion du menu flottant

        private ICommand _relayCommandAddItem;
        public ICommand RelayCommandAddItem
        {
            get
            {
                return _relayCommandAddItem;
            }
            set
            {
                _relayCommandAddItem = value;
            }
        }

        private void AddItem(object param)
        {
            PlaylistViewModel playlist = param as PlaylistViewModel;

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = true;
            dialog.ShowDialog();
            List<string> files = new List<string>(dialog.FileNames);


            foreach (IMedia media in FileManager.CreateMedias(files, playlist))
            {
                playlist.Items.Add(media);
            }
        }

        private ICommand _relayCommandDeletePlaylist;
        public ICommand RelayCommandDeletePlaylist
        {
            get
            {
                return _relayCommandDeletePlaylist;
            }
            set
            {
                _relayCommandDeletePlaylist = value;
            }
        }

        private void DeletePlaylist(object param)
        {
            Playlists.Remove(param as PlaylistViewModel);
        }

        #endregion

        #region Init

        private void InitRelay()
        {
            //Gestions boutons
            _relayCommandButtonPrev = new RelayCommand(new Action(Prev));
            _relayCommandButtonNext = new RelayCommand(new Action(Next));
            _relayCommandButtonNewPlayList = new RelayCommand(new Action(NewPlayList));

            //Gestions Search TextBox
            _relayCommandSearchDone = new RelayCommand<object>(new Action<object>(SearchDone));

            //Gestion du menu flottant
            _relayCommandAddItem = new RelayCommand<object>(new Action<object>(AddItem));
            _relayCommandDeletePlaylist = new RelayCommand<object>(new Action<object>(DeletePlaylist));

            //Selection de la playlist dans la listbox
            _relayCommandPlaylistClicked = new RelayCommand<object>(new Action<object>(PlaylistClicked));

            //Gestion du thème
            _relayCommandButtonBlue = new RelayCommand(new Action(SetBlue));
            _relayCommandButtonGreen = new RelayCommand(new Action(SetGreen));
            _relayCommandButtonRed = new RelayCommand(new Action(SetRed));

            //Gestion de la fermeture de la fenêtre
            _relayCommandWindowClosed = new RelayCommand(new Action(Close));

            //Gestion play music
            _relayCommandPlayMusic = new RelayCommand<object>(new Action<object>(PlayMusic));

            //Gestion play video
            _relayCommandPlayVideo = new RelayCommand<object>(new Action<object>(PlayVideo));

            //Gestion play video
            _relayCommandDisplayImage = new RelayCommand<object>(new Action<object>(DisplayImage));
        }

        private void InitMessenger()
        {
            Messenger.Default.Register<NewPlayListMessage>(this, new Action<NewPlayListMessage>(OnCreateNewPlayList));
            Messenger.Default.Register<RequestNewPlaylistMessage>(this, new Action<RequestNewPlaylistMessage>(OnRequestAddPlaylist));
            Messenger.Default.Register<RequestNextItem>(this, new Action<RequestNextItem>(OnNextItemRequested));
        }

        public void Load()
        {
            List<PlaylistViewModel> list = FileManager.LoadFromXml();

            foreach (PlaylistViewModel elem in list)
            {
                Playlists.Add(elem);
            }
        }

        #endregion

        #region Gestion Play music

        private ICommand _relayCommandPlayMusic;
        public ICommand RelayCommandPlayMusic
        {
            get
            {
                return _relayCommandPlayMusic;
            }
            set
            {
                _relayCommandPlayMusic = value;
            }
        }

        private void PlayMusic(object obj)
        {
            if (obj != null)
            {
                _itemPlaying = obj as IMedia;
                _playlistPlaying = _playlistMusicSelected;
                Messenger.Default.Send<PlayMessage>(new PlayMessage(_itemPlaying.Name, "Music", _itemPlaying.Path));
            }
        }

        #endregion

        #region Gestion Play video

        private ICommand _relayCommandPlayVideo;
        public ICommand RelayCommandPlayVideo
        {
            get
            {
                return _relayCommandPlayVideo;
            }
            set
            {
                _relayCommandPlayVideo = value;
            }
        }

        private void PlayVideo(object obj)
        {
            if (obj != null)
            {
                _itemPlaying = obj as IMedia;
                _playlistPlaying = _playlistVideoSelected;
                Messenger.Default.Send<PlayMessage>(new PlayMessage(_itemPlaying.Name, "Video", _itemPlaying.Path));
            }
        }

        #endregion

        #region Gestion Display image

        private ICommand _relayCommandDisplayImage;
        public ICommand RelayCommandDisplayImage
        {
            get
            {
                return _relayCommandDisplayImage;
            }
            set
            {
                _relayCommandDisplayImage = value;
            }
        }

        private void DisplayImage(object obj)
        {
            if (obj != null)
            {
                _itemPlaying = obj as IMedia;
                _playlistPlaying = _playlistImageSelected;
                Messenger.Default.Send<PlayMessage>(new PlayMessage(_itemPlaying.Name, "Image", _itemPlaying.Path));
            }
        }

        #endregion

        private void OnNextItemRequested(RequestNextItem e)
        {
            int i = 0;

            foreach (IMedia elem in _playlistPlaying.Items)
            {
                if (elem == _itemPlaying)
                {
                    if (i + 1 < _playlistPlaying.Items.Count)
                    {
                        _itemPlaying = _playlistPlaying.Items[i + 1];
                        Messenger.Default.Send<PlayMessage>(new PlayMessage(_itemPlaying.Name, "ALL", _itemPlaying.Path));
                        return;
                    }
                }
                i++;
            }
        }

        private void Prev()
        {
            int i = 0;

            foreach (IMedia elem in _playlistPlaying.Items)
            {
                if (elem == _itemPlaying)
                {
                    if (i - 1 >= 0)
                    {
                        _itemPlaying = _playlistPlaying.Items[i - 1];
                        Messenger.Default.Send<PlayMessage>(new PlayMessage(_itemPlaying.Name, "ALL", _itemPlaying.Path));
                        return;
                    }
                }
                i++;
            }
        }

        private void Next()
        {
            int i = 0;

            foreach (IMedia elem in _playlistPlaying.Items)
            {
                if (elem == _itemPlaying)
                {
                    if (i + 1 < _playlistPlaying.Items.Count)
                    {
                        _itemPlaying = _playlistPlaying.Items[i + 1];
                        Messenger.Default.Send<PlayMessage>(new PlayMessage(_itemPlaying.Name, "ALL", _itemPlaying.Path));
                        return;
                    }
                }
                i++;
            }
        }

        public MainViewModel()
        {
            InitRelay();
            InitMessenger();
            Load();

            _background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.LightBlue);
        }

        #region Gestion Close

        private ICommand _relayCommandWindowClosed;
        public ICommand RelayCommandWindowClosed
        {
            get
            {
                return _relayCommandWindowClosed;
            }
            set
            {
                _relayCommandWindowClosed = value;
            }
        }

        private void Close()
        {
            FileManager.SaveInXml(new List<PlaylistViewModel>(Playlists));
        }

        #endregion
    }
}