﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Messaging;
using MyWindowsMediaPlayer.Message;

namespace MyWindowsMediaPlayer
{
    /// <summary>
    /// Interaction logic for AddPlaylistView.xaml
    /// </summary>
    public partial class AddPlaylistView : Window
    {
        public AddPlaylistView()
        {
            InitializeComponent();
            Messenger.Default.Register<CloseWindowMessage>(this, new Action<CloseWindowMessage>(OnClosedWindowMessage));
        }

        public void OnClosedWindowMessage(CloseWindowMessage e)
        {
            if (e.SendWindowInfos == true)
            {
                Messenger.Default.Send<NewPlayListMessage>(new NewPlayListMessage(e.Name, e.Type));
                Messenger.Default.Unregister(this, new Action<CloseWindowMessage>(OnClosedWindowMessage));
                this.Close();
            }
            else
            {
                this.Close();
            }
        }
    }
}