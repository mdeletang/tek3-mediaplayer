﻿using GalaSoft.MvvmLight.Messaging;
using MyWindowsMediaPlayer.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MyWindowsMediaPlayer
{
    public partial class WindowFullScreen : Window
    {
        public WindowFullScreen(TimeSpan pos, Uri path)
        {
            InitializeComponent();
            PlayerFullScreen.Source = path;
            PlayerFullScreen.Position = pos;
            PlayerFullScreen.LoadedBehavior = MediaState.Play;
            this.Closing += WindowFullScreen_Closing;
        }

        private void WindowFullScreen_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Messenger.Default.Send<CloseFullWindowMessage>(new CloseFullWindowMessage(PlayerFullScreen.Position));
        }

        private void PlayerFullScreen_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                Close();
            }
        }
    }
}
