﻿using MyWindowsMediaPlayer.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyWindowsMediaPlayer.Model
{
    public class SerializablePlaylistVideo
    { 
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlArray(ElementName = "Items")]
        [XmlArrayItem(ElementName = "Item")]
        public List<Video> Items { get; set; }
        [XmlElement(ElementName = "Editable")]
        public bool Editable { get; set; }

        public SerializablePlaylistVideo(PlaylistViewModel model)
        {
            Name = model.Name;
            Items = new List<Video>();
            Editable = model.Editable;
            List<IMedia> tmp = new List<IMedia>(model.Items);

            Items = tmp.ConvertAll(o => (Video)o);
        }

        public SerializablePlaylistVideo()
        {
            Items = new List<Video>();
        }
    }
}