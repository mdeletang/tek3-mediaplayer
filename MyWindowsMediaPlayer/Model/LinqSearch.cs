﻿using MyWindowsMediaPlayer.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyWindowsMediaPlayer.Model
{
    public static class LinqSearch
    {
        public static List<IMedia> WhenCalled(String searchedWord, List<PlaylistViewModel> allPlaylists)
        {
            List<IMedia> queryResult = new List<IMedia>();
            foreach (PlaylistViewModel playlist in allPlaylists)
            {
                var query = from a in playlist.Items
                            where a.Name == searchedWord
                            select a;
                queryResult.AddRange(query);
            }
            return (queryResult);
        }
    }
}