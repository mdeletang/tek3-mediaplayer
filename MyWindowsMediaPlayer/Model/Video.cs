﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MyWindowsMediaPlayer.Model
{
    public class Video : IMedia
    {
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "Path")]
        public string Path { get; set; }
        [XmlElement(ElementName = "Mark")]
        public int Mark { get; set; }
        [XmlElement(ElementName = "Length")]
        public double Length { get; set; }

        public Video(string path)
        {
            Name = string.Empty;
            Path = path;
        }

        public Video()
        {
            Name = string.Empty;
        }
    }
}
