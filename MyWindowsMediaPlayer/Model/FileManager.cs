﻿using MyWindowsMediaPlayer.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyWindowsMediaPlayer.Model
{
    public static class FileManager
    {
        public static string PathVideo = @"C:\Users\marcd_000\Desktop\saveVideo.xml";
        public static string PathMusic = @"C:\Users\marcd_000\Desktop\saveMusic.xml";
        public static string PathImage = @"C:\Users\marcd_000\Desktop\saveImage.xml";

        //A VOIR SI COMPLETER
        private static List<string> _videoExtensions = new List<string>()
        {
            ".mp4",
            ".avi",
            ".wmv",
            "",
        };

        private static List<string> _musicExtensions = new List<string>()
        {
            ".mp3",
            "",
        };

        private static List<string> _imageExtensions = new List<string>()
        {
            ".png",
            ".jpg",
            ""
        };

        #region Gestion chargement

        private static List<PlaylistViewModel> LoadVideo()
        {
            List<PlaylistViewModel> ret = new List<PlaylistViewModel>();
            List<SerializablePlaylistVideo> list = new List<SerializablePlaylistVideo>();
            bool found = false;

            try
            {
                XmlSerializer xs = new XmlSerializer(typeof(List<SerializablePlaylistVideo>), new XmlRootAttribute("MediaPlayerSave"));
                if (File.Exists(PathVideo))
                using (Stream stream = new FileStream(PathVideo, FileMode.Open))
                {
                    list = (List<SerializablePlaylistVideo>)xs.Deserialize(stream);
                }
            }
            catch
            {
                return null;
            }
            foreach (SerializablePlaylistVideo elem in list)
                ret.Add(new PlaylistViewModel(elem));
            foreach (PlaylistViewModel elem in ret)
            {
                if (elem.Name == "My videos")
                    found = true;
            }
            if (found == false)
                ret.Add(new PlaylistViewModel("My videos", typeof(Video), false));
            return ret;
        }

        private static List<PlaylistViewModel> LoadMusic()
        {
            List<PlaylistViewModel> ret = new List<PlaylistViewModel>();
            List<SerializablePlaylistMusic> list = new List<SerializablePlaylistMusic>();
            bool found = false;

            try
            {
                XmlSerializer xs = new XmlSerializer(typeof(List<SerializablePlaylistMusic>), new XmlRootAttribute("MediaPlayerSave"));
                if (File.Exists(PathMusic))
                    using (Stream stream = new FileStream(PathMusic, FileMode.Open))
                    {
                        list = (List<SerializablePlaylistMusic>)xs.Deserialize(stream);
                    }
            }
            catch
            {
                return null;
            }
            foreach (SerializablePlaylistMusic elem in list)
                ret.Add(new PlaylistViewModel(elem));
            foreach (PlaylistViewModel elem in ret)
            {
                if (elem.Name == "My musics")
                    found = true;
            }
            if (found == false)
                ret.Add(new PlaylistViewModel("My musics", typeof(Music), false));
            return ret;
        }

        private static List<PlaylistViewModel> LoadImage()
        {
            List<PlaylistViewModel> ret = new List<PlaylistViewModel>();
            List<SerializablePlaylistImage> list = new List<SerializablePlaylistImage>();
            bool found = false;

            try
            {
                XmlSerializer xs = new XmlSerializer(typeof(List<SerializablePlaylistImage>), new XmlRootAttribute("MediaPlayerSave"));
                if (File.Exists(PathImage))
                using (Stream stream = new FileStream(PathImage, FileMode.Open))
                {
                    list = (List<SerializablePlaylistImage>)xs.Deserialize(stream);
                }
            }
            catch
            {
                return null;
            }
            foreach (SerializablePlaylistImage elem in list)
                ret.Add(new PlaylistViewModel(elem));
            foreach (PlaylistViewModel elem in ret)
            {
                if (elem.Name == "My images")
                    found = true;
            }
            if (found == false)
                ret.Add(new PlaylistViewModel("My images", typeof(Image), false));
            return ret;
        }

        public static List<PlaylistViewModel> LoadFromXml()
        {
            List<PlaylistViewModel> ret = new List<PlaylistViewModel>();
            List<PlaylistViewModel> tmp;

            tmp = LoadMusic();
            if (tmp != null)
                foreach (PlaylistViewModel elem in tmp)
                {
                    ret.Add(elem);
                }
            tmp = LoadVideo();
            if (tmp != null)
                foreach (PlaylistViewModel elem in tmp)
                {
                    ret.Add(elem);
                }
            tmp = LoadImage();
            if (tmp != null)
                foreach (PlaylistViewModel elem in tmp)
                {
                    ret.Add(elem);
                }
            return ret;
        }

        #endregion

        #region Gestion sauvegarde

        private static bool SaveVideo(List<PlaylistViewModel> models)
        {
            List<SerializablePlaylistVideo> playlists = new List<SerializablePlaylistVideo>();

            foreach (PlaylistViewModel elem in models)
            {
                if (elem.DataType == typeof(Video))
                    playlists.Add(new SerializablePlaylistVideo(elem));
            }
            try
            {
                if (File.Exists(PathVideo))
                    File.Delete(PathVideo);
                XmlSerializer xs = new XmlSerializer(typeof(List<SerializablePlaylistVideo>), new XmlRootAttribute("MediaPlayerSave"));
                using (StreamWriter stream = new StreamWriter(PathVideo))
                {
                    xs.Serialize(stream, playlists);
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        private static bool SaveMusic(List<PlaylistViewModel> models)
        {
            List<SerializablePlaylistMusic> playlists = new List<SerializablePlaylistMusic>();

            foreach (PlaylistViewModel elem in models)
            {
                if (elem.DataType == typeof(Music))
                    playlists.Add(new SerializablePlaylistMusic(elem));
            }
            try
            {
                if (File.Exists(PathMusic))
                    File.Delete(PathMusic);
                XmlSerializer xs = new XmlSerializer(typeof(List<SerializablePlaylistMusic>), new XmlRootAttribute("MediaPlayerSave"));
                using (StreamWriter stream = new StreamWriter(PathMusic))
                {
                    xs.Serialize(stream, playlists);
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        private static bool SaveImage(List<PlaylistViewModel> models)
        {
            List<SerializablePlaylistImage> playlists = new List<SerializablePlaylistImage>();

            foreach (PlaylistViewModel elem in models)
            {
                if (elem.DataType == typeof(Image))
                    playlists.Add(new SerializablePlaylistImage(elem));
            }
            try
            {
                if (File.Exists(PathImage))
                    File.Delete(PathImage);
                XmlSerializer xs = new XmlSerializer(typeof(List<SerializablePlaylistImage>), new XmlRootAttribute("MediaPlayerSave"));
                using (StreamWriter stream = new StreamWriter(PathImage))
                {
                    xs.Serialize(stream, playlists);
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static bool SaveInXml(List<PlaylistViewModel> models)
        {
            int count = 0;
            if (SaveMusic(models))
                ++count;
            if (SaveVideo(models))
                ++count;
            if (SaveImage(models))
                ++count;
            if (count == 3)
                return true;
            return false;
        }

        #endregion


        //Gestion de la création des Media, en fonction des extensions de fichiers
        public static List<IMedia> CreateMedias(List<string> filesPaths, PlaylistViewModel playlist)
        {
            List<IMedia> ret = new List<IMedia>();
            Dictionary<string, string> fileNames = new Dictionary<string, string>();
            List<string> extension;

            foreach (string filePath in filesPaths)
            {
                int lastOcc = filePath.LastIndexOf('\\');
                if (lastOcc != -1)
                    fileNames.Add(filePath, filePath.Substring(lastOcc + 1));
            }
            if (playlist.DataType == typeof(Video))
                extension = _videoExtensions;
            else if (playlist.DataType == typeof(Image))
                extension = _imageExtensions;
            else if (playlist.DataType == typeof(Music))
                extension = _musicExtensions;
            else
                return new List<IMedia>();

            foreach (KeyValuePair<string, string> keyVal in fileNames)
            {
                string path = keyVal.Key;
                string name = keyVal.Value;

                int lastOcc = name.LastIndexOf('.');

                if (lastOcc != -1)
                    if (extension.Contains(name.Substring(lastOcc)))
                    {
                        try
                        {
                            IMedia item = Activator.CreateInstance(playlist.DataType, path) as IMedia;

                            if (item != null)
                            {
                                item.Name = name.Substring(0, name.LastIndexOf('.'));
                                item.Mark = 0;
                            }
                            ret.Add(item);
                        }
                        catch
                        {
                            //:s:s
                        }
                    }
            }
            return ret;
        }

        static public List<Uri> ScanDirectory(string path, Type type)
        {
            var mediaFiles = new List<Uri>();
            Dictionary<Type, string[]> supportedTypes = new Dictionary<Type,string[]>();
            string[] musicTypes = { ".mp3", ".ogg", ".wav" }; //liste sommaire à completer plus tard
            string[] videoTypes = { ".avi", ".mp4", ".mkv" }; 
            string[] imageTypes = { ".jpg", ".jpeg", ".png", ".bmp" };
            supportedTypes.Add(typeof(Music), musicTypes);
            supportedTypes.Add(typeof(Video), videoTypes);
            supportedTypes.Add(typeof(Image), imageTypes);

            if (System.IO.Directory.Exists(path))
            {
                var subDirs = new List<String>();
                var files = new List<String>();

                try
                {
                    subDirs = Directory.GetDirectories(path).ToList();
                }
                catch { }
                try
                {
                    files = Directory.GetFiles(path).ToList();
                }
                catch { }
                foreach (string entry in subDirs)
                {
                    var results = ScanDirectory(entry, type);
                    mediaFiles = mediaFiles.Union(results).ToList();
                }

                foreach (string file in files)
                {
                    if (supportedTypes[type].Contains(Path.GetExtension(file)))
                    {
                        Console.WriteLine(file);
                        mediaFiles.Add(new Uri(path));
                    }
                }
                return mediaFiles;
            }
            return null;
        }
    }
}
