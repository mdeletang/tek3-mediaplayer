﻿using MyWindowsMediaPlayer.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyWindowsMediaPlayer.Model
{
    public class SerializablePlaylistMusic
    {
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlArray(ElementName = "Items")]
        [XmlArrayItem(ElementName = "Item")]
        public List<Music> Items { get; set; }
        [XmlElement(ElementName = "Editable")]
        public bool Editable { get; set; }

        public SerializablePlaylistMusic(PlaylistViewModel model)
        {
            Name = model.Name;
            Items = new List<Music>();
            Editable = model.Editable;
            List<IMedia> tmp = new List<IMedia>(model.Items);

            Items = tmp.ConvertAll(o => (Music)o);
        }

        public SerializablePlaylistMusic()
        {
            Items = new List<Music>();
        }
    }
}
