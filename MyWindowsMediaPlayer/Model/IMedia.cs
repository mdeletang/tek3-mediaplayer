﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MyWindowsMediaPlayer.Model
{
    public interface IMedia
    {
        string Name { get; set; }
        string Path { get; set; }
        int Mark { get; set; }
    }
}
