﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MyWindowsMediaPlayer.Model
{
    public class Music : IMedia
    {
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "Path")]
        public string Path { get; set; }
        [XmlElement(ElementName = "Mark")]
        public int Mark { get; set; }
        [XmlElement(ElementName = "Length")]
        public double Length { get; set; }
        [XmlElement(ElementName = "Artist")]
        public string Artist { get; set; }
        [XmlElement(ElementName = "Album")]
        public string Album { get; set; }
        [XmlElement(ElementName = "Genre")]
        public string Genre { get; set; }

        public Music(string path)
        {
            Name = string.Empty;
            Artist = string.Empty;
            Album = string.Empty;
            Genre = string.Empty;
            Path = path;
        }

        public Music()
        {
            Name = string.Empty;
            Artist = string.Empty;
            Album = string.Empty;
            Genre = string.Empty;
            Path = string.Empty;
        }
    }
}
