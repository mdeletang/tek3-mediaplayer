﻿using MyWindowsMediaPlayer.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyWindowsMediaPlayer.Model
{
    public class SerializablePlaylistImage
    { 
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlArray(ElementName = "Items")]
        [XmlArrayItem(ElementName = "Item")]
        public List<Image> Items { get; set; }
        [XmlElement(ElementName = "Editable")]
        public bool Editable { get; set; }

        public SerializablePlaylistImage(PlaylistViewModel model)
        {
            Name = model.Name;
            Items = new List<Image>();
            Editable = model.Editable;
            List<IMedia> tmp = new List<IMedia>(model.Items);

            Items = tmp.ConvertAll(o => (Image)o);
        }

        public SerializablePlaylistImage()
        {
            Items = new List<Image>();
        }
    }
}