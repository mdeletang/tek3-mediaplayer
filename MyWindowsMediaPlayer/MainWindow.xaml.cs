﻿using System.Windows;
using MyWindowsMediaPlayer.ViewModel;
using GalaSoft.MvvmLight.Messaging;
using MyWindowsMediaPlayer.Message;
using MahApps.Metro.Controls;
using System;
using MahApps.Metro;
using System.Windows.Controls;
using System.Timers;

namespace MyWindowsMediaPlayer
{
    [CLSCompliant(false)]
    public partial class MainWindow : MetroWindow
    {
        private Accent _blueSkin = ThemeManager.DefaultAccents[8];
        private Accent _greenSkin = ThemeManager.DefaultAccents[1];
        private Accent _redSkin = ThemeManager.DefaultAccents[0];
        private Accent _current;
        private Timer _timer = new Timer();
        private String _currentListViewType;

        #region Gestion des couleurs
        private void BluePickerClick(object sender, RoutedEventArgs e)
        {
            ComboBoxColors.SelectedItem = ComboBoxColors.Items[0];
            ComboBoxColors.IsDropDownOpen = false;
            if (_current != _blueSkin)
            {
                ThemeManager.ChangeTheme(this, _blueSkin, Theme.Light);
                _current = _blueSkin;
            }
        }
        private void GreenPickerClick(object sender, RoutedEventArgs e)
        {
            ComboBoxColors.SelectedItem = ComboBoxColors.Items[1];
            ComboBoxColors.IsDropDownOpen = false;
            if (_current != _greenSkin)
            {
                ThemeManager.ChangeTheme(this, _greenSkin, Theme.Light);
                _current = _greenSkin;
            }
        }
        private void RedPickerClick(object sender, RoutedEventArgs e)
        {
            ComboBoxColors.SelectedItem = ComboBoxColors.Items[2];
            ComboBoxColors.IsDropDownOpen = false;
            if (_current != _redSkin)
            {
                ThemeManager.ChangeTheme(this, _redSkin, Theme.Light);
                _current = _redSkin;
            }
        }
        #endregion

        #region Gestion slider time

        private delegate void SetSliderTimedelegate();
        private SetSliderTimedelegate delegateSliderTime;
        private void SetSliderTime()
        {
            Duration durationTotal = MediaPlayer.NaturalDuration;
            TimeSpan duration = MediaPlayer.Position;
            double total = 0;
            double pos = duration.TotalSeconds;
            string sduration;
            string sdurationTotal;

            if (durationTotal.HasTimeSpan)
                total = durationTotal.TimeSpan.TotalSeconds;
            else
                return;
            if (total > 0 || pos > 0)
                SliderTime.Value = 100 * pos / total;
            else
                SliderTime.Value = 0;
            sduration = duration.ToString();
            sdurationTotal = durationTotal.TimeSpan.ToString();
            if (sduration.Contains("."))
                sduration = sduration.Substring(0, sduration.LastIndexOf('.'));
            if (sdurationTotal.Contains("."))
                sdurationTotal = sdurationTotal.Substring(0, sdurationTotal.LastIndexOf('.'));
            TextBlockMediaDuration.Text = sduration + " / " + sdurationTotal;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (Application.Current != null)
                Application.Current.Dispatcher.BeginInvoke(delegateSliderTime);
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Duration durationTotal = MediaPlayer.NaturalDuration;
            TimeSpan pos = MediaPlayer.Position;
            double sliderVal;
            double mediaVal;

            if (!durationTotal.HasTimeSpan)
                return;
            mediaVal = pos.TotalMilliseconds;
            sliderVal = e.NewValue * durationTotal.TimeSpan.TotalMilliseconds / 100;
            if (sliderVal - mediaVal > 200 || sliderVal - mediaVal < -200)
            {
                _timer.Stop();
                MediaPlayer.LoadedBehavior = MediaState.Pause;
                MediaPlayer.Position = TimeSpan.FromMilliseconds(sliderVal);
                MediaPlayer.LoadedBehavior = MediaState.Play;
                _timer.Start();
            }
        }

        #endregion

        #region Gestion bouton play

        private bool _isPlaying = false;
        private void ButtonPlay_Click(object sender, RoutedEventArgs e)
        {
            if (_isPlaying == false)
            {
                if (MediaPlayer.Source != null)
                {
                    ButtonPlay.Content = "||";
                    _isPlaying = true;
                    MediaPlayer.LoadedBehavior = MediaState.Play;
                }
            }
            else
            {
                if (MediaPlayer.Source != null)
                {
                    ButtonPlay.Content = ">";
                    _isPlaying = false;
                    MediaPlayer.LoadedBehavior = MediaState.Pause;
                }
            }
        }

        #endregion

        public MainWindow()
        {
            InitializeComponent();
            Closing += (s, e) => ViewModelLocator.Cleanup();
            ThemeManager.ChangeTheme(this, _blueSkin, Theme.Light);
            Messenger.Default.Register<PlayMessage>(this, new Action<PlayMessage>(OnRequestPlay));
            Messenger.Default.Register<ChangeVisibilityMessage>(this, new Action<ChangeVisibilityMessage>(ChangeVisibility));
            Messenger.Default.Register<CloseFullWindowMessage>(this, new Action<CloseFullWindowMessage>(OnEndOfFullScreen));
            MediaPlayer.LoadedBehavior = MediaState.Pause;
            _current = _blueSkin;

            delegateSliderTime = new SetSliderTimedelegate(SetSliderTime);
            _timer = new Timer();
            _timer.Elapsed += Timer_Elapsed;
            _timer.Interval = 100;
            _timer.Start();
        }

        private void ChangeVisibility(ChangeVisibilityMessage obj)
        {
            ListViewMusics.Visibility = obj.Music ? Visibility.Visible : Visibility.Collapsed;
            ListViewVideos.Visibility = obj.Video ? Visibility.Visible : Visibility.Collapsed;
            ListViewImages.Visibility = obj.Image ? Visibility.Visible : Visibility.Collapsed;
            MediaPlayer.Visibility = Visibility.Collapsed;
        }

        private void OnRequestPlay(PlayMessage e)
        {
            MediaPlayer.Source = new Uri(e.Path);
            MediaPlayer.LoadedBehavior = MediaState.Play;
            TextBlockMediaName.Text = e.Name;
            ButtonPlay.Content = "||";
            _isPlaying = true;
            _currentListViewType = e.Type;
            if (e.Type != "Music")
            {
                MediaPlayer.Visibility = Visibility.Visible;
                ButtonMaximise.Visibility = Visibility.Visible;
                GetListViewType(e.Type).Visibility = Visibility.Collapsed;
            }
        }

        private void MediaPlayer_MediaEnded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send<RequestNextItem>(new RequestNextItem());
        }

        private void MediaPlayer_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            MessageBox.Show("Can't open file");
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ButtonMaximise_Click(object sender, RoutedEventArgs e)
        {
            if (MediaPlayer.Visibility == Visibility.Collapsed)
            {
                MediaPlayer.Visibility = Visibility.Visible;
                ListViewMusics.Visibility = Visibility.Collapsed;
                ListViewVideos.Visibility = Visibility.Collapsed;
                ListViewImages.Visibility = Visibility.Collapsed;
            }
            else
            {
                MediaPlayer.Visibility = Visibility.Collapsed;
                if (_currentListViewType != "Music")
                    GetListViewType(_currentListViewType).Visibility = Visibility.Visible;
            }
        }

        private ListView GetListViewType(String type)
        {
            return (type == "Video" ? ListViewVideos : ListViewImages);
        }

        private void OnEndOfFullScreen(CloseFullWindowMessage obj)
        {
            MediaPlayer.Position = obj.Position;
            MediaPlayer.LoadedBehavior = MediaState.Play;
        }

        private void MediaPlayer_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                MediaPlayer.LoadedBehavior = MediaState.Pause;
                WindowFullScreen screen = new WindowFullScreen(MediaPlayer.Position, MediaPlayer.Source);
                screen.ShowDialog();
            }
        }
    }
}