﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyWindowsMediaPlayer.ViewModel;

namespace MyWindowsMediaPlayer.Message
{
    public class DeletePlaylistMessage
    {
        public PlaylistViewModel PlayListToDelete { get; set; }

        public DeletePlaylistMessage(PlaylistViewModel elem)
        {
            Console.WriteLine(elem.GetType());
            PlayListToDelete = elem;
        }
    }
}
