﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyWindowsMediaPlayer.Message
{
    public class RequestNewPlaylistMessage
    {
        public string PlayListName { get; set; }

        public RequestNewPlaylistMessage(string name)
        {
            PlayListName = name;
        }
    }
}