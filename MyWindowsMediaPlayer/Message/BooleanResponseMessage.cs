﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyWindowsMediaPlayer
{
    public class BooleanResponseMessage
    {
        public string MessageData { get; set; }
        public bool Response { get; set; }

        public BooleanResponseMessage(bool response)
        {
            Response = response;
        }
    }
}
