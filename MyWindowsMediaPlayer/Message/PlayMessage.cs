﻿using MyWindowsMediaPlayer.Model;
using MyWindowsMediaPlayer.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyWindowsMediaPlayer.Message
{
    public class PlayMessage
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Path { get; set; }

        public PlayMessage(string name, string type, string path)
        {
            Name = name;
            Type = type;
            Path = path;
        }
    }
}
