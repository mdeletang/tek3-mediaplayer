﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyWindowsMediaPlayer.Message
{
    public class CloseFullWindowMessage
    {
        public TimeSpan Position { get; set; }

        public CloseFullWindowMessage(TimeSpan position)
        {
            Position = position;
        }
    }
}
