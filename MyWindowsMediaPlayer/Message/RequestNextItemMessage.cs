﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyWindowsMediaPlayer.Message
{
    public class RequestNextItemMessage
    {
        public Type Type { get; set; }

        public RequestNextItemMessage()
        {

        }

        public RequestNextItemMessage(Type type)
        {
            Type = type;
        }
    }
}
