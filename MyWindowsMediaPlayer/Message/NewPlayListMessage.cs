﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyWindowsMediaPlayer.Message
{
    public class NewPlayListMessage
    {
        public Type Type { get; set; }
        public string Name { get; set; }

        public NewPlayListMessage(string name, Type type)
        {
            Type = type;
            Name = name;
        }
    }
}
