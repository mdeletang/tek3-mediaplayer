﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyWindowsMediaPlayer.Message
{
    public class ChangeVisibilityMessage
    {
        public bool Music { get; set; }
        public bool Video { get; set; }
        public bool Image { get; set; }

        public ChangeVisibilityMessage(bool music, bool video, bool image)
        {
            Music = music;
            Video = video;
            Image = image;
        }
    }
}
