﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyWindowsMediaPlayer.Message
{
    public class CloseWindowMessage
    {
        public bool SendWindowInfos { get; set; }
        public string Name { get; set; }
        public Type Type { get; set; }
    }
}
