﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyWindowsMediaPlayer.Message
{
    public class RequestNextItem
    {
        public string Type { get; set; }

        public RequestNextItem(string type)
        {
            Type = type;
        }

        public RequestNextItem()
        {

        }
    }
}
